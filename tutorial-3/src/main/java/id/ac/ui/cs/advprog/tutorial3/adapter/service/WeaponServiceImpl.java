package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    private boolean initialized;

    // feel free to include more repositories if you think it might help :)
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;

    public void init() {
        weaponRepository.findAll().forEach((weapon) -> {
            this.weaponRepository.save(weapon);
        });
        bowRepository.findAll().forEach((bow) -> {
            this.weaponRepository.save(new BowAdapter(bow));
        });
        spellbookRepository.findAll().forEach((spellbook) -> {
            this.weaponRepository.save(new SpellbookAdapter(spellbook));
        });

        initialized = true;
    }

    public boolean getInitStatus() {
        return this.initialized;
    }

    @Override
    public List<Weapon> findAll() {
        if (!initialized) init();
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        if (!initialized) init();
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if (attackType == 0) {
            logRepository.addLog(weapon.normalAttack());
        } else {
            logRepository.addLog(weapon.chargedAttack());
        }
        weaponRepository.save(weapon);

    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }

}
