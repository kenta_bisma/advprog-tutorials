package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean hasPower = true;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        hasPower = true;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (hasPower) {
            hasPower = false;
            return spellbook.largeSpell();
        }
        return "Power still recharging!";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
