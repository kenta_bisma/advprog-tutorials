package id.ac.ui.cs.advprog.tutorial3.adapter.controller;

import id.ac.ui.cs.advprog.tutorial3.adapter.service.WeaponService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/battle")
public class AdapterController {

    @Autowired
    private WeaponService weaponService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public String battleHome(Model model) {
        model.addAttribute("weapons", weaponService.findAll());

        List<String> logs = weaponService.getAllLogs();
        model.addAttribute("logs", logs);
        return "adapter/home";
    }

    @RequestMapping(path = "/attack")
    public String attackWithWeapon(@RequestParam(value = "weaponName") String weaponName,
            @RequestParam(value = "attackType") int attackType) {
        weaponService.attackWithWeapon(weaponName, attackType);
        return "redirect:/battle";
    }
}
