package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class DraconicTransformation extends CelestialTransformation {

    public DraconicTransformation(DragonPredicate predicate) {
        super(predicate.dragonKey);
    }

    public static enum DragonPredicate {
        DVALIN("erstwhilekingoftheskies"),
        VISHAP("earthshakerdragon"),
        ENDER("emperoroftheend"),
        VIPER("netherdrake"),
        JAKIRO("fireandice"),
        AURELION_SOL("starforger"),
        YU_ZHONG("fighterimba");

        public String dragonKey;

        private DragonPredicate(String name) {
            Spell spell = new Spell(name, AlphaCodex.getInstance());
            this.dragonKey = (new AbyssalTransformation()).encode(spell).getText();
        }

    }
}
