package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        this.type = AdventurerType.MYSTIC;
    }

    @Override
    public void update() {
        System.out.println("the mystic one are on their way!");
        getQuestFromGuild();
    }

}
