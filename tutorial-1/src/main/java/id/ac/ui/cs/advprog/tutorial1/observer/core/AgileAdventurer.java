package id.ac.ui.cs.advprog.tutorial1.observer.core;


public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        this.type = AdventurerType.AGILE;
    }

    @Override
    public void update() {
        System.out.println("The agile one are on their way!");
        getQuestFromGuild();
    }

}
