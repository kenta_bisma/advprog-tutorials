package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.List;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer.AdventurerType;

public class Quest {
    private String title;
    private String type;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() { return this.type; }

    public static enum QuestType {
        DELIVERY(AdventurerType.KNIGHT, AdventurerType.AGILE, AdventurerType.MYSTIC),
        RUMBLE(AdventurerType.KNIGHT, AdventurerType.AGILE),
        ESCORT(AdventurerType.KNIGHT, AdventurerType.MYSTIC);

        private List<AdventurerType> types;

        private QuestType(AdventurerType... types) {
            this.types = List.of(types);
        }

        public static List<AdventurerType> getAdventurerTypes(String type) {
            if (type.charAt(0) == 'D') {
                return DELIVERY.types;
            } else if (type.charAt(0) == 'R') {
                return RUMBLE.types;
            } else {
                return ESCORT.types;
            }
            
        }
    }
}
