package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        this.type = AdventurerType.KNIGHT;
    }

    @Override
    public void update() {
        System.out.println("the knight are on their way!");
        getQuestFromGuild();
    }

}
