package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Quest.QuestType;

public abstract class Adventurer {
    protected Guild guild;
    protected String name;
    protected AdventurerType type;
    private List<Quest> quests = new ArrayList<>();

    public abstract void update();

    public String getName() {
        return this.name;
    }

    public List<Quest> getQuests() {
        return this.quests;
    }

    protected void getQuestFromGuild() {
        Quest quest = guild.getQuest();
        if (QuestType.getAdventurerTypes(quest.getType()).contains(this.type))
            this.getQuests().add(guild.getQuest());
    }

    public static enum AdventurerType {
        AGILE, KNIGHT, MYSTIC;
    }
}
