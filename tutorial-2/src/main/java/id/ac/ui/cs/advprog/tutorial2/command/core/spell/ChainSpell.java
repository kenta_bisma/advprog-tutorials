package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
public class ChainSpell implements Spell {

    private ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        spells.forEach( (spell) -> {
            spell.cast();
        });
    }

    @Override
    public void undo() {
        for (int i = spells.size(); i > 0; --i) {
            spells.get(i).undo();
        }
    }
}
